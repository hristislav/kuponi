alias Server.{Repo, Accounts, Addressess}
alias Server.Accounts.{Child, Parent, School, Credential}
alias ServerWeb.Router.Helpers, as: Routes

try do
  Addressess.get_city!(1)
  :ok
rescue
  Ecto.NoResultsError ->
    Mock.init()
  end
  # Mock.init()

# rado = Accounts.get_parent!(1)|> Repo.preload(:children)
# denica = Accounts.get_child!(1)
# radostin = Accounts.get_parent!(1) |> Repo.preload(:children)
# denica = %Child{}|> Child.changeset(%{name: "Denica Radostinova Dimitrova"})
# qwor = %Child{}|> Child.changeset(%{name: "Qwor Radostinov Dimitrov"})
#
#  Ecto.build_assoc(radostin,:children,denica) |> Repo.insert!
#  Accounts.get_parent!(1) |> Repo.preload(:children)
#
#  Ecto.build_assoc(Accounts.get_parent!(1),:children,qwor) |> Repo.insert!
#  Accounts.get_parent!(1) |> Repo.preload(:children)
# Accounts.create_credential(%{username: "username",password_hash: "1234",type: :parent})
# c = Accounts.get_credential! 1

# cities = ["Айтос","Аксаково","Алфатар","Антоново","Априлци","Ардино","Асеновград","Ахелой","Ахтопол","Балчик","Банкя","Банско","Баня","Батак","Батановци","Белене","Белица","Белово","Белоградчик","Белослав","Берковица","Благоевград","Бобов дол","Бобошево","Божурище","Бойчиновци","Болярово","Борово","Ботевград","Брацигово","Брегово","Брезник","Брезово","Брусарци","Бургас","Бухово","Българово","Бяла (област Варна)","Бяла (област Русе)","Бяла Слатина","Бяла черква","Варна","Велики Преслав","Велико Търново","Велинград","Ветово","Ветрен","Видин","Враца","Вълчедръм","Вълчи дол","Върбица","Вършец","Габрово","Генерал Тошево","Главиница","Глоджево","Годеч","Горна Оряховица","Гоце Делчев","Грамада","Гулянци","Гурково","Гълъбово","Две могили","Дебелец","Девин","Девня","Джебел","Димитровград","Димово","Добринище","Добрич","Долна баня","Долна Митрополия","Долна Оряховица","Долни Дъбник","Долни чифлик","Доспат","Драгоман","Дряново","Дулово","Дунавци","Дупница","Дългопол","Елена","Елин Пелин","Елхово","Етрополе","Завет","Земен","Златарица","Златица","Златоград","Ивайловград‎","Игнатиево","Искър","Исперих","Ихтиман","Каблешково","Каварна","Казанлък","Калофер","Камено","Каолиново","Карлово","Карнобат","Каспичан","Кермен","Килифарево","Китен","Клисура","Кнежа","Козлодуй","Койнаре","Копривщица","Костандово","Костенец","Костинброд","Котел","Кочериново","Кресна","Криводол","Кричим","Крумовград","Крън","Кубрат","Куклен","Кула","Кърджали","Кюстендил","Левски","Летница","Ловеч","Лозница","Лом","Луковит","Лъки","Любимец","Лясковец","Мадан","Маджарово","Малко Търново","Мартен","Мелник","Мездра","Меричлери","Мизия","Момин проход","Момчилград","Монтана","Мъглиж","Неделино","Несебър","Николаево","Никопол","Нова Загора","Нови Искър","Нови пазар","Обзор","Омуртаг","Опака","Оряхово","Павел баня","Павликени","Пазарджик","Панагюрище","Перник","Перущица","Петрич","Пещера","Пирдоп","Плачковци","Плевен","Плиска","Пловдив","Полски Тръмбеш","Поморие","Попово","Пордим","Правец","Приморско","Провадия","Първомай","Раднево","Радомир","Разград","Разлог","Ракитово","Раковски","Рила","Роман","Рудозем","Русе","Садово","Самоков","Сандански","Сапарева баня","Свети Влас","Свиленград","Свищов","Своге","Севлиево","Сеново","Септември","Силистра","Симеоновград","Симитли","Славяново","Сливен","Сливница","Сливо поле","Смолян","Смядово","Созопол","Сопот","София","Средец","Стамболийски","Стара Загора","Стражица","Стралджа","Стрелча","Суворово","Сунгурларе","Сухиндол","Съединение","Сърница","Твърдица","Тервел","Тетевен","Тополовград","Троян","Трън","Тръстеник","Трявна","Тутракан","Търговище","Угърчин","Хаджидимово","Харманли","Хасково","Хисаря","Цар Калоян","Царево","Чепеларе","Червен бряг","Черноморец","Чипровци","Чирпан","Шабла","Шивачево","Шипка","Шумен","Ябланица","Якоруда","Ямбол"]
# for c <- cities do
# Addressess.create_city(%{name: c})
# end

# schools = ["Начално училище 'Васил Левски'","Основно училище 'Антон Страшимиров'","Основно училище 'Васил Априлов'","Основно училище 'Георги Сава Раковски'","Основно училище 'Добри Чинтулов'","Основно училище 'Захари Стоянов'","Основно училище 'Иван Вазов'","Основно училище 'Свети Иван Рилски'","Основно училище 'Йордан Йовков'","Основно училище 'Константин Арабаджиев'","Основно училище 'Добри Войников'","Основно училище 'Капитан Петко войвода'","Основно училище 'Отец Паисий'","Основно училище 'Панайот Волов'","Основно училище 'Свети Патриарх Евтимий'","Основно училище 'Петко Рачов Славейков'","Основно училище 'Свети свети Кирил и Методий'","Основно училище 'Стефан Караджа'","Основно училище 'Стоян Михайловски'","Основно училище 'Христо Ботев'","Основно училище 'Христо Смирненски'","Основно училище 'Цар Симеон І'","Основно училище 'Черноризец Храбър'","Седмо средно училище 'Найден Геров'","Средно училище с езиково обучение 'Ал. С. Пушкин'","Основно училище 'Васил Друмев'","Специализирано спортно училище 'Георги Бенковски'","Средно училище 'Гео Милев'","Средно училище 'Елин Пелин'","Средно училище 'Любен Каравелов'","Средно училище 'Пейо Крачолов Яворов'","Средно училище 'Свети Климент Охридски'","Трета природоматематическа гимназия 'Академик Методий Попов'","ІV Езикова гимназия 'Фредерик Жолио - Кюри'","Гимназия с преподаване на чужди езици 'Йоан Екзарх'","Професионална гимназия по икономика 'Д-р Иван Богоров'","Първа езикова гимназия","Математическа гимназия 'Д-р Петър Берон'","Средно училище за хуманитарни науки и изкуства 'Константин Преславски'","Специално училище за ученици с нарушено зрение 'Проф. д-р Иван Шишманов'","Професионална техническа гимназия","Професионална гимназия по химични и хранително-вкусови технологии 'Дм.Ив.Менделеев'","Частно основно училище 'Аз съм българче'","Частна професионална гимназия по инженеринг, технологии и компютърни науки","Професионална гимназия по строителство, архитектура и геодезия 'Васил Левски'","Частно основно училище 'Прогресивно образование'","Частна търговска гимназия 'Конто Трейд'","Частна професионална гимназия по управление на туризма 'Константин Фотинов'","Частна профилирана гимназия 'Антоан дьо Сент-Екзюпери'","Частно основно училище 'Малкият принц'","Частна езикова гимназия с изучаване на чужди езици 'Джордж Байрон'","Основно училище 'Свети Климент Охридски'","Търговска гимназия 'Георги Стойков Раковски'","Професионална гимназия по текстил и моден дизайн","Професионална гимназия по горско стопанство и дървообработване 'Николай Хайтов'","Морска гимназия 'Свети Николай Чудотворец'","Професионална гимназия по електротехника","Професионална гимназия по туризъм 'Професор д-р Асен Златаров'","І Основно училище 'Свети княз Борис I'","III Oсновно училище 'Ангел Кънчев'","Средно училище 'Димчо Дебелянов'","Средно училище 'Неофит Бозвели'","Второ основно училище 'Никола Йонков Вапцаров'","Частно средно училище 'Мечтатели'","Частно основно училище 'Талант'","Частно основно училище 'Д-р Мария Монтесори'","Частна професионална гимназия по мода и модна стилистика 'Богоя'","Национално училище по изкуствата 'Добри Христов'"]
# for s <- schools do
# Addressess.create_city(%{name: s})
# end
