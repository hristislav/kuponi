defmodule Server.AddressesTest do
  use Server.DataCase

  alias Server.Addresses

  describe "addressess" do
    alias Server.Addresses.Address

    @valid_attrs %{
      address: "some address",
      city: "some city",
      country: "some country",
      post_code: "some post_code",
      province: "some province"
    }
    @update_attrs %{
      address: "some updated address",
      city: "some updated city",
      country: "some updated country",
      post_code: "some updated post_code",
      province: "some updated province"
    }
    @invalid_attrs %{address: nil, city: nil, country: nil, post_code: nil, province: nil}

    def address_fixture(attrs \\ %{}) do
      {:ok, address} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Addresses.create_address()

      address
    end

    test "list_addressess/0 returns all addressess" do
      address = address_fixture()
      assert Addresses.list_addressess() == [address]
    end

    test "get_address!/1 returns the address with given id" do
      address = address_fixture()
      assert Addresses.get_address!(address.id) == address
    end

    test "create_address/1 with valid data creates a address" do
      assert {:ok, %Address{} = address} = Addresses.create_address(@valid_attrs)
      assert address.address == "some address"
      assert address.city == "some city"
      assert address.country == "some country"
      assert address.post_code == "some post_code"
      assert address.province == "some province"
    end

    test "create_address/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Addresses.create_address(@invalid_attrs)
    end

    test "update_address/2 with valid data updates the address" do
      address = address_fixture()
      assert {:ok, %Address{} = address} = Addresses.update_address(address, @update_attrs)
      assert address.address == "some updated address"
      assert address.city == "some updated city"
      assert address.country == "some updated country"
      assert address.post_code == "some updated post_code"
      assert address.province == "some updated province"
    end

    test "update_address/2 with invalid data returns error changeset" do
      address = address_fixture()
      assert {:error, %Ecto.Changeset{}} = Addresses.update_address(address, @invalid_attrs)
      assert address == Addresses.get_address!(address.id)
    end

    test "delete_address/1 deletes the address" do
      address = address_fixture()
      assert {:ok, %Address{}} = Addresses.delete_address(address)
      assert_raise Ecto.NoResultsError, fn -> Addresses.get_address!(address.id) end
    end

    test "change_address/1 returns a address changeset" do
      address = address_fixture()
      assert %Ecto.Changeset{} = Addresses.change_address(address)
    end
  end

  describe "cities" do
    alias Server.Addresses.City

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def city_fixture(attrs \\ %{}) do
      {:ok, city} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Addresses.create_city()

      city
    end

    test "list_cities/0 returns all cities" do
      city = city_fixture()
      assert Addresses.list_cities() == [city]
    end

    test "get_city!/1 returns the city with given id" do
      city = city_fixture()
      assert Addresses.get_city!(city.id) == city
    end

    test "create_city/1 with valid data creates a city" do
      assert {:ok, %City{} = city} = Addresses.create_city(@valid_attrs)
      assert city.name == "some name"
    end

    test "create_city/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Addresses.create_city(@invalid_attrs)
    end

    test "update_city/2 with valid data updates the city" do
      city = city_fixture()
      assert {:ok, %City{} = city} = Addresses.update_city(city, @update_attrs)
      assert city.name == "some updated name"
    end

    test "update_city/2 with invalid data returns error changeset" do
      city = city_fixture()
      assert {:error, %Ecto.Changeset{}} = Addresses.update_city(city, @invalid_attrs)
      assert city == Addresses.get_city!(city.id)
    end

    test "delete_city/1 deletes the city" do
      city = city_fixture()
      assert {:ok, %City{}} = Addresses.delete_city(city)
      assert_raise Ecto.NoResultsError, fn -> Addresses.get_city!(city.id) end
    end

    test "change_city/1 returns a city changeset" do
      city = city_fixture()
      assert %Ecto.Changeset{} = Addresses.change_city(city)
    end
  end
end
