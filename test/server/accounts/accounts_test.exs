defmodule Server.AccountsTest do
  use Server.DataCase

  alias Server.Accounts

  describe "credentials" do
    alias Server.Accounts.Credential

    @valid_attrs %{
      password_hash: "some password_hash",
      type: "some type",
      username: "some username"
    }
    @update_attrs %{
      password_hash: "some updated password_hash",
      type: "some updated type",
      username: "some updated username"
    }
    @invalid_attrs %{password_hash: nil, type: nil, username: nil}

    def credential_fixture(attrs \\ %{}) do
      {:ok, credential} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_credential()

      credential
    end

    test "list_credentials/0 returns all credentials" do
      credential = credential_fixture()
      assert Accounts.list_credentials() == [credential]
    end

    test "get_credential!/1 returns the credential with given id" do
      credential = credential_fixture()
      assert Accounts.get_credential!(credential.id) == credential
    end

    test "create_credential/1 with valid data creates a credential" do
      assert {:ok, %Credential{} = credential} = Accounts.create_credential(@valid_attrs)
      assert credential.password_hash == "some password_hash"
      assert credential.type == "some type"
      assert credential.username == "some username"
    end

    test "create_credential/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_credential(@invalid_attrs)
    end

    test "update_credential/2 with valid data updates the credential" do
      credential = credential_fixture()

      assert {:ok, %Credential{} = credential} =
               Accounts.update_credential(credential, @update_attrs)

      assert credential.password_hash == "some updated password_hash"
      assert credential.type == "some updated type"
      assert credential.username == "some updated username"
    end

    test "update_credential/2 with invalid data returns error changeset" do
      credential = credential_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_credential(credential, @invalid_attrs)
      assert credential == Accounts.get_credential!(credential.id)
    end

    test "delete_credential/1 deletes the credential" do
      credential = credential_fixture()
      assert {:ok, %Credential{}} = Accounts.delete_credential(credential)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_credential!(credential.id) end
    end

    test "change_credential/1 returns a credential changeset" do
      credential = credential_fixture()
      assert %Ecto.Changeset{} = Accounts.change_credential(credential)
    end
  end

  describe "parents" do
    alias Server.Accounts.Parent

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def parent_fixture(attrs \\ %{}) do
      {:ok, parent} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_parent()

      parent
    end

    test "list_parents/0 returns all parents" do
      parent = parent_fixture()
      assert Accounts.list_parents() == [parent]
    end

    test "get_parent!/1 returns the parent with given id" do
      parent = parent_fixture()
      assert Accounts.get_parent!(parent.id) == parent
    end

    test "create_parent/1 with valid data creates a parent" do
      assert {:ok, %Parent{} = parent} = Accounts.create_parent(@valid_attrs)
      assert parent.name == "some name"
    end

    test "create_parent/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_parent(@invalid_attrs)
    end

    test "update_parent/2 with valid data updates the parent" do
      parent = parent_fixture()
      assert {:ok, %Parent{} = parent} = Accounts.update_parent(parent, @update_attrs)
      assert parent.name == "some updated name"
    end

    test "update_parent/2 with invalid data returns error changeset" do
      parent = parent_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_parent(parent, @invalid_attrs)
      assert parent == Accounts.get_parent!(parent.id)
    end

    test "delete_parent/1 deletes the parent" do
      parent = parent_fixture()
      assert {:ok, %Parent{}} = Accounts.delete_parent(parent)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_parent!(parent.id) end
    end

    test "change_parent/1 returns a parent changeset" do
      parent = parent_fixture()
      assert %Ecto.Changeset{} = Accounts.change_parent(parent)
    end
  end

  describe "schools" do
    alias Server.Accounts.School

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def school_fixture(attrs \\ %{}) do
      {:ok, school} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_school()

      school
    end

    test "list_schools/0 returns all schools" do
      school = school_fixture()
      assert Accounts.list_schools() == [school]
    end

    test "get_school!/1 returns the school with given id" do
      school = school_fixture()
      assert Accounts.get_school!(school.id) == school
    end

    test "create_school/1 with valid data creates a school" do
      assert {:ok, %School{} = school} = Accounts.create_school(@valid_attrs)
      assert school.name == "some name"
    end

    test "create_school/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_school(@invalid_attrs)
    end

    test "update_school/2 with valid data updates the school" do
      school = school_fixture()
      assert {:ok, %School{} = school} = Accounts.update_school(school, @update_attrs)
      assert school.name == "some updated name"
    end

    test "update_school/2 with invalid data returns error changeset" do
      school = school_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_school(school, @invalid_attrs)
      assert school == Accounts.get_school!(school.id)
    end

    test "delete_school/1 deletes the school" do
      school = school_fixture()
      assert {:ok, %School{}} = Accounts.delete_school(school)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_school!(school.id) end
    end

    test "change_school/1 returns a school changeset" do
      school = school_fixture()
      assert %Ecto.Changeset{} = Accounts.change_school(school)
    end
  end

  describe "children" do
    alias Server.Accounts.Child

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def child_fixture(attrs \\ %{}) do
      {:ok, child} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_child()

      child
    end

    test "list_children/0 returns all children" do
      child = child_fixture()
      assert Accounts.list_children() == [child]
    end

    test "get_child!/1 returns the child with given id" do
      child = child_fixture()
      assert Accounts.get_child!(child.id) == child
    end

    test "create_child/1 with valid data creates a child" do
      assert {:ok, %Child{} = child} = Accounts.create_child(@valid_attrs)
      assert child.name == "some name"
    end

    test "create_child/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_child(@invalid_attrs)
    end

    test "update_child/2 with valid data updates the child" do
      child = child_fixture()
      assert {:ok, %Child{} = child} = Accounts.update_child(child, @update_attrs)
      assert child.name == "some updated name"
    end

    test "update_child/2 with invalid data returns error changeset" do
      child = child_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_child(child, @invalid_attrs)
      assert child == Accounts.get_child!(child.id)
    end

    test "delete_child/1 deletes the child" do
      child = child_fixture()
      assert {:ok, %Child{}} = Accounts.delete_child(child)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_child!(child.id) end
    end

    test "change_child/1 returns a child changeset" do
      child = child_fixture()
      assert %Ecto.Changeset{} = Accounts.change_child(child)
    end
  end

  describe "caterings" do
    alias Server.Accounts.Catering

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def catering_fixture(attrs \\ %{}) do
      {:ok, catering} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_catering()

      catering
    end

    test "list_caterings/0 returns all caterings" do
      catering = catering_fixture()
      assert Accounts.list_caterings() == [catering]
    end

    test "get_catering!/1 returns the catering with given id" do
      catering = catering_fixture()
      assert Accounts.get_catering!(catering.id) == catering
    end

    test "create_catering/1 with valid data creates a catering" do
      assert {:ok, %Catering{} = catering} = Accounts.create_catering(@valid_attrs)
      assert catering.name == "some name"
    end

    test "create_catering/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_catering(@invalid_attrs)
    end

    test "update_catering/2 with valid data updates the catering" do
      catering = catering_fixture()
      assert {:ok, %Catering{} = catering} = Accounts.update_catering(catering, @update_attrs)
      assert catering.name == "some updated name"
    end

    test "update_catering/2 with invalid data returns error changeset" do
      catering = catering_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_catering(catering, @invalid_attrs)
      assert catering == Accounts.get_catering!(catering.id)
    end

    test "delete_catering/1 deletes the catering" do
      catering = catering_fixture()
      assert {:ok, %Catering{}} = Accounts.delete_catering(catering)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_catering!(catering.id) end
    end

    test "change_catering/1 returns a catering changeset" do
      catering = catering_fixture()
      assert %Ecto.Changeset{} = Accounts.change_catering(catering)
    end
  end

  describe "child_confirmations" do
    alias Server.Accounts.ChildConfirmation

    @valid_attrs %{
      confirmation_code: "some confirmation_code",
      egn: "some egn",
      secure_code: "some secure_code"
    }
    @update_attrs %{
      confirmation_code: "some updated confirmation_code",
      egn: "some updated egn",
      secure_code: "some updated secure_code"
    }
    @invalid_attrs %{confirmation_code: nil, egn: nil, secure_code: nil}

    def child_confirmation_fixture(attrs \\ %{}) do
      {:ok, child_confirmation} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_child_confirmation()

      child_confirmation
    end

    test "list_child_confirmations/0 returns all child_confirmations" do
      child_confirmation = child_confirmation_fixture()
      assert Accounts.list_child_confirmations() == [child_confirmation]
    end

    test "get_child_confirmation!/1 returns the child_confirmation with given id" do
      child_confirmation = child_confirmation_fixture()
      assert Accounts.get_child_confirmation!(child_confirmation.id) == child_confirmation
    end

    test "create_child_confirmation/1 with valid data creates a child_confirmation" do
      assert {:ok, %ChildConfirmation{} = child_confirmation} =
               Accounts.create_child_confirmation(@valid_attrs)

      assert child_confirmation.confirmation_code == "some confirmation_code"
      assert child_confirmation.egn == "some egn"
      assert child_confirmation.secure_code == "some secure_code"
    end

    test "create_child_confirmation/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_child_confirmation(@invalid_attrs)
    end

    test "update_child_confirmation/2 with valid data updates the child_confirmation" do
      child_confirmation = child_confirmation_fixture()

      assert {:ok, %ChildConfirmation{} = child_confirmation} =
               Accounts.update_child_confirmation(child_confirmation, @update_attrs)

      assert child_confirmation.confirmation_code == "some updated confirmation_code"
      assert child_confirmation.egn == "some updated egn"
      assert child_confirmation.secure_code == "some updated secure_code"
    end

    test "update_child_confirmation/2 with invalid data returns error changeset" do
      child_confirmation = child_confirmation_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Accounts.update_child_confirmation(child_confirmation, @invalid_attrs)

      assert child_confirmation == Accounts.get_child_confirmation!(child_confirmation.id)
    end

    test "delete_child_confirmation/1 deletes the child_confirmation" do
      child_confirmation = child_confirmation_fixture()
      assert {:ok, %ChildConfirmation{}} = Accounts.delete_child_confirmation(child_confirmation)

      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_child_confirmation!(child_confirmation.id)
      end
    end

    test "change_child_confirmation/1 returns a child_confirmation changeset" do
      child_confirmation = child_confirmation_fixture()
      assert %Ecto.Changeset{} = Accounts.change_child_confirmation(child_confirmation)
    end
  end

  describe "hosts" do
    alias Server.Accounts.Host

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def host_fixture(attrs \\ %{}) do
      {:ok, host} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_host()

      host
    end

    test "list_hosts/0 returns all hosts" do
      host = host_fixture()
      assert Accounts.list_hosts() == [host]
    end

    test "get_host!/1 returns the host with given id" do
      host = host_fixture()
      assert Accounts.get_host!(host.id) == host
    end

    test "create_host/1 with valid data creates a host" do
      assert {:ok, %Host{} = host} = Accounts.create_host(@valid_attrs)
      assert host.name == "some name"
    end

    test "create_host/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_host(@invalid_attrs)
    end

    test "update_host/2 with valid data updates the host" do
      host = host_fixture()
      assert {:ok, %Host{} = host} = Accounts.update_host(host, @update_attrs)
      assert host.name == "some updated name"
    end

    test "update_host/2 with invalid data returns error changeset" do
      host = host_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_host(host, @invalid_attrs)
      assert host == Accounts.get_host!(host.id)
    end

    test "delete_host/1 deletes the host" do
      host = host_fixture()
      assert {:ok, %Host{}} = Accounts.delete_host(host)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_host!(host.id) end
    end

    test "change_host/1 returns a host changeset" do
      host = host_fixture()
      assert %Ecto.Changeset{} = Accounts.change_host(host)
    end
  end

  describe "administrators" do
    alias Server.Accounts.Administrator

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def administrator_fixture(attrs \\ %{}) do
      {:ok, administrator} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_administrator()

      administrator
    end

    test "list_administrators/0 returns all administrators" do
      administrator = administrator_fixture()
      assert Accounts.list_administrators() == [administrator]
    end

    test "get_administrator!/1 returns the administrator with given id" do
      administrator = administrator_fixture()
      assert Accounts.get_administrator!(administrator.id) == administrator
    end

    test "create_administrator/1 with valid data creates a administrator" do
      assert {:ok, %Administrator{} = administrator} = Accounts.create_administrator(@valid_attrs)
      assert administrator.name == "some name"
    end

    test "create_administrator/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_administrator(@invalid_attrs)
    end

    test "update_administrator/2 with valid data updates the administrator" do
      administrator = administrator_fixture()

      assert {:ok, %Administrator{} = administrator} =
               Accounts.update_administrator(administrator, @update_attrs)

      assert administrator.name == "some updated name"
    end

    test "update_administrator/2 with invalid data returns error changeset" do
      administrator = administrator_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Accounts.update_administrator(administrator, @invalid_attrs)

      assert administrator == Accounts.get_administrator!(administrator.id)
    end

    test "delete_administrator/1 deletes the administrator" do
      administrator = administrator_fixture()
      assert {:ok, %Administrator{}} = Accounts.delete_administrator(administrator)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_administrator!(administrator.id) end
    end

    test "change_administrator/1 returns a administrator changeset" do
      administrator = administrator_fixture()
      assert %Ecto.Changeset{} = Accounts.change_administrator(administrator)
    end
  end
end
