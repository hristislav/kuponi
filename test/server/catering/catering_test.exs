defmodule Server.CateringTest do
  use Server.DataCase

  alias Server.Catering

  describe "food_types" do
    alias Server.Catering.FoodType

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def food_type_fixture(attrs \\ %{}) do
      {:ok, food_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catering.create_food_type()

      food_type
    end

    test "list_food_types/0 returns all food_types" do
      food_type = food_type_fixture()
      assert Catering.list_food_types() == [food_type]
    end

    test "get_food_type!/1 returns the food_type with given id" do
      food_type = food_type_fixture()
      assert Catering.get_food_type!(food_type.id) == food_type
    end

    test "create_food_type/1 with valid data creates a food_type" do
      assert {:ok, %FoodType{} = food_type} = Catering.create_food_type(@valid_attrs)
      assert food_type.name == "some name"
    end

    test "create_food_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catering.create_food_type(@invalid_attrs)
    end

    test "update_food_type/2 with valid data updates the food_type" do
      food_type = food_type_fixture()
      assert {:ok, %FoodType{} = food_type} = Catering.update_food_type(food_type, @update_attrs)
      assert food_type.name == "some updated name"
    end

    test "update_food_type/2 with invalid data returns error changeset" do
      food_type = food_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Catering.update_food_type(food_type, @invalid_attrs)
      assert food_type == Catering.get_food_type!(food_type.id)
    end

    test "delete_food_type/1 deletes the food_type" do
      food_type = food_type_fixture()
      assert {:ok, %FoodType{}} = Catering.delete_food_type(food_type)
      assert_raise Ecto.NoResultsError, fn -> Catering.get_food_type!(food_type.id) end
    end

    test "change_food_type/1 returns a food_type changeset" do
      food_type = food_type_fixture()
      assert %Ecto.Changeset{} = Catering.change_food_type(food_type)
    end
  end

  describe "foods" do
    alias Server.Catering.Food

    @valid_attrs %{description: "some description", name: "some name", price: 120.5}
    @update_attrs %{
      description: "some updated description",
      name: "some updated name",
      price: 456.7
    }
    @invalid_attrs %{description: nil, name: nil, price: nil}

    def food_fixture(attrs \\ %{}) do
      {:ok, food} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catering.create_food()

      food
    end

    test "list_foods/0 returns all foods" do
      food = food_fixture()
      assert Catering.list_foods() == [food]
    end

    test "get_food!/1 returns the food with given id" do
      food = food_fixture()
      assert Catering.get_food!(food.id) == food
    end

    test "create_food/1 with valid data creates a food" do
      assert {:ok, %Food{} = food} = Catering.create_food(@valid_attrs)
      assert food.description == "some description"
      assert food.name == "some name"
      assert food.price == 120.5
    end

    test "create_food/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catering.create_food(@invalid_attrs)
    end

    test "update_food/2 with valid data updates the food" do
      food = food_fixture()
      assert {:ok, %Food{} = food} = Catering.update_food(food, @update_attrs)
      assert food.description == "some updated description"
      assert food.name == "some updated name"
      assert food.price == 456.7
    end

    test "update_food/2 with invalid data returns error changeset" do
      food = food_fixture()
      assert {:error, %Ecto.Changeset{}} = Catering.update_food(food, @invalid_attrs)
      assert food == Catering.get_food!(food.id)
    end

    test "delete_food/1 deletes the food" do
      food = food_fixture()
      assert {:ok, %Food{}} = Catering.delete_food(food)
      assert_raise Ecto.NoResultsError, fn -> Catering.get_food!(food.id) end
    end

    test "change_food/1 returns a food changeset" do
      food = food_fixture()
      assert %Ecto.Changeset{} = Catering.change_food(food)
    end
  end
end
