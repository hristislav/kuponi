defmodule ServerWeb.Router do
  use ServerWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  pipeline :auth do
    plug(Server.Auth.Pipeline)
  end

  scope "/", ServerWeb do
    pipe_through([:browser, :auth])

    get("/", PageController, :index)
    resources("/us__ers", Us__erController)

    get("/login", SessionController, :login)
    get("/register", ParentController, :registration)
    get("/logout", SessionController, :delete)
    resources("/menus", MenuController, only: [:index])
    resources("/menu", MenuController, except: [:index])
    resources("/sessions", SessionController, only: [:new, :create, :delete], singleton: true)

    resources("/administrators", AdministratorController)

    # done
    resources "/parents", ParentController do
      # done
      resources("/wallet", ParentWalletController, expect: [:index])

      resources("/orders", OrderController)
      get("/delete_order", OrderController, :delete_order)

      # done
      resources("/children", ParentChildController)

      # resources "/orders", ParentOrderController
    end

    resources "/caterings", CateringController do
      get("/orders", OrderController, :orders)
      # done
      resources("/menus", CateringMenuController)
      # done
      resources("/foods", CateringFoodController)
    end

    # done
    resources "/schools", SchoolController do
      # done
      resources("/children", SchoolChildController)
    end

    # done - show page to be finished when bracelet is checked
    resources("/hosts", HostController)
  end

  # Other scopes may use custom stacks.
  # scope "/api", ServerWeb do
  #   pipe_through :api
  # end
end
