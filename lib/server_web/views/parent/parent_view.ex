defmodule ServerWeb.ParentView do
  use ServerWeb, :view

  def name(string_full_name, key) do
    [first_name, middle_name, last_name] =
      case String.split(string_full_name, " ") do
        [_first_name] = name -> name ++ ["Empty surname", "Empty family name"]
        [_first_name, _middle_name] = name -> name ++ ["Empty family name"]
        [_first_name, _middle_name, _last_name] = name -> name
        [firstname, middlename, lastname | _] -> [firstname, middlename, lastname]
      end

    Map.get(
      %{first_name: first_name, middle_name: middle_name, last_name: last_name},
      key
    )
  end

  # def name(_, _), do: "#{__MODULE__}.name/2 => Wrong name format or key."
end
