defmodule ServerWeb.SchoolChildView do
  use ServerWeb, :view

  def gen_secure_code() do
    <<secure_code_confirmation::binary-size(12), _::binary>> =
      Hash.hash(:crypto.strong_rand_bytes(256))

    secure_code_confirmation
  end
end
