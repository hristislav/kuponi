defmodule ServerWeb.MenuController do
  use ServerWeb, :controller

  alias Server.Catering
  alias Server.Repo

  def index(conn, params) do
    menus = Catering.list_menus
        
    list=
        for x <- menus, into: [] do
            x
            |> Repo.preload(:food_dessert)
            |> Repo.preload(:food_main)
            |> Repo.preload(:food_starter)
            |> Repo.preload(:catering)
        end
    
    render(conn, "index.html", menus: list, child_id: params["child_id"], parent_id: params["parent_id"], school_id: params["school_id"])
  end

end
