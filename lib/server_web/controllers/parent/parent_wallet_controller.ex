defmodule ServerWeb.ParentWalletController do
  use ServerWeb, :controller

  # def index(conn, params) do
  #   parent = Server.Accounts.get_parent!(params["parent_id"])
  #   render(conn, "index.html", parent: parent)
  # end

  def new(conn, _params) do
    render(conn, "index.html")
  end

  def create(conn, _params) do
    render(conn, "index.html")
  end

  def show(conn, params) do
    parent = Server.Accounts.get_parent!(params["parent_id"])
    render(conn, "show.html", parent: parent)
  end

  def edit(conn, _params) do
    render(conn, "index.html")
  end

  def update(conn, params) do
    id = params["id"]
    money = params["update"]["money"]["money"]
    {balance, rest} = Float.parse(money)
    parent = Server.Accounts.get_parent!(id)
    final = balance + parent.money
    updated = Server.Accounts.update_parent(parent, %{"money" => Float.to_string(final)})

    parent = Server.Accounts.get_parent!(params["parent_id"])

    conn
    |> put_flash(:info, "Упешно добавени пари в акаунта!")
    |> render("show.html", parent: parent)
    |> halt()
  end

  def delete(conn, _params) do
    render(conn, "index.html")
  end
end
