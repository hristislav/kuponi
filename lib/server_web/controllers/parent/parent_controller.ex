defmodule ServerWeb.ParentController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.Parent

  def index(conn, _params) do
    parents = Accounts.list_parents()
    Print.term(conn,"---------------------Paretn Conn---------------------")
    render(conn, "index.html", parents: parents)
  end

  def new(conn, _params) do
    cities = Server.Addressess.list_cities(:names)

    render(conn, "registration.html", cities: cities)
  end

  # def create(conn, %{"parent" => parent_params}) do
  def create(conn, params) do
    parent_params =
      params
      |> get_in(~w(registration parent)s)
      |> put_in(~w(credential type)s, "parent")

    case Accounts.create_parent(parent_params) do
      {:ok, _parent_details} ->
        conn
        |> put_flash(:info, "Успешна регистрация!")
        |> redirect(to: Routes.parent_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Грешка при регистрацията!")

        render("new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    render(conn, "show.html", parent: parent)
  end

  def edit(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    changeset = Accounts.change_parent(parent)
    render(conn, "edit.html", parent: parent, changeset: changeset)
  end

  def update(conn, %{"id" => id, "parent" => parent_params}) do
    parent = Accounts.get_parent!(id)

    case Accounts.update_parent(parent, parent_params) do
      {:ok, parent} ->
        conn
        |> put_flash(:info, "Успешно обновена информация!")
        |> redirect(to: Routes.parent_path(conn, :show, parent))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Грешка при обновяването на информация!")
        |> redirect(to: Routes.parent_path(conn, :index))
    end
  end

  def delete(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    {:ok, _parent} = Accounts.delete_parent(parent)

    conn
    |> put_flash(:info, "Изтриването е успешно!")
    |> redirect(to: Routes.parent_path(conn, :index))
  end

  def registration(conn, _) do
    cities = Server.Addressess.list_cities(:names)
    render(conn, "registration.html", cities: cities)
  end
end
