defmodule ServerWeb.SessionController do
  use ServerWeb, :controller

  alias Server.{Accounts, Addressess}
  alias Server.Accounts.{Credential, Parent}
  alias Server.Auth.Guardian
  import Comeonin.Bcrypt

  def login(conn, _params) do
    render(conn, "login.html")
  end

  def logout(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> put_flash(:success, "Logout !")
    |> redirect(to: "/")
  end

  def create(conn, %{"login" => %{"credential" => %{"email" => email, "password" => password}}}) do
    conn1 =
      conn
      |> Accounts.sign_in(email, password)

    case conn1 do
      {:error, err} ->
        conn
        |> put_flash(:error, "#{err}")
        |> redirect(to: "/")
        |> halt()

      %Plug.Conn{} ->
        conn1
        |> put_flash(:info, "Logged succesfully!")
        |> redirect(to: Routes.page_path(conn1, :index))
        |> halt()
    end
  end

  def delete(conn, _) do
    conn
    |> configure_session(drop: true)
    |> put_flash(:success, "Logout !")
    |> Guardian.Plug.sign_out()
    |> redirect(to: "/")
  end
end
