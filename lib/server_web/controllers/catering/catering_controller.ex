defmodule ServerWeb.CateringController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.Catering
  alias Server.Addressess
  alias Server.Repo
  alias Server.Accounts.Credential

  def index(conn, _params) do
    conn
    |> assign(:cities, Addressess.list_cities(:names))
    |> assign(:caterings, Accounts.list_caterings())
    |> render("index.html")
  end

  def new(conn, _params) do
    conn
    |> assign(:cities, Addressess.list_cities(:names))
    |> assign(:changeset, Accounts.change_catering())
    |> render("new.html")
  end

  def create(conn, %{"catering" => catering_params} = _params) do
    case Accounts.create_catering(catering_params) do
      {:ok, %Catering{} = catering} ->
        conn
        |> put_flash(:info, "Упешно регистриран акаунт!")
        |> redirect(to: Routes.catering_path(conn, :show, catering))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Грешка при регистрация!")
        |> assign(:cities, Addressess.list_cities(:names))
        |> assign(:changeset, changeset)
        |> render("new.html")
    end
  end

  def delete(conn, %{"id" => id} = params) do
    {:ok, _catering} =
      id
      |> Accounts.get_catering!()
      |> Accounts.delete_catering()

    conn
    |> put_flash(:info, "Успешно изтрита кетъринг фирма.")
    |> redirect(to: Routes.catering_path(conn, :index))
  end

  def show(conn, %{"id" => id} = params) do
    conn
    |> assign(:catering, Accounts.get_catering!(id))
    |> render("show.html")
  end

  def edit(conn, %{"id" => id} = params) do
    catering =
      Accounts.get_catering!(id)
      |> Repo.preload(:address)

    assign = [
      catering: catering,
      changeset: Accounts.change_catering(catering),
      cities: Addressess.list_cities(:names)
    ]

    render(conn, "edit.html", assign)
  end

  def update(conn, %{"id" => id, "catering" => catering_params} = params) do
    catering =
      id
      |> Accounts.get_catering!()
      |> Repo.preload(:address)

    case Accounts.update_catering(catering, catering_params) do
      {:ok, updated_catering} ->
        new_assign_conn =
          conn
          |> assign(:catering, updated_catering)
          |> assign(:changeset, updated_catering)
          |> assign(:cities, Addressess.list_cities(:names))

        new_assign_conn
        |> put_flash(:info, "Успешно обновена информация.")
        |> redirect(to: Routes.catering_path(new_assign_conn, :show, catering))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", catering: catering, changeset: changeset)
    end
  end

  defp parse_cities(cities) do
    Enum.map(cities, fn %Server.Addressess.Address{id: id, city: name} -> {name, id} end)
  end
end
