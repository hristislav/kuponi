defmodule ServerWeb.CateringMenuController do
  use ServerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html", menus: Server.Catering.list_menus)
  end

  def new(conn, id) do
    menu = Server.Catering.change_menu(%Server.Catering.Menu{})
    id = id["catering_id"]
    render(conn, "new.html", menu: menu, id: id, starters: Server.Catering.get_starters(id), mains: Server.Catering.get_main(id), desserts: Server.Catering.get_dessert(id))
  end

  def create(conn, params) do

    catering_id = params["catering_id"]
    date = params["menu"]["date"]
    dessert = params["menu"]["dessert"]
    main = params["menu"]["main"]
    starter = params["menu"]["starter"]
    map = %{
      date: date,
      dessert_id: String.to_integer(dessert),
      main_id: String.to_integer(main),
      starter_id: String.to_integer(starter),
      catering_id: String.to_integer(catering_id)
    }
    Server.Catering.new_menu(map)
    render(conn, "index.html", menus: Server.Catering.list_menus)
  end

  def show(conn, params) do
    id = params["id"]
    menu = Server.Catering.get_menu(id)
    render(conn, "show.html", menu: menu)
  end

  def edit(conn, %{"catering_id" => cid, "menu_id" => mid} = params) do
    redirect(conn, to: Routes.catering_catering_menu_path(conn, :edit, cid, mid, params))
  end

  def update(conn, %{"catering_id" => cid, "menu_id" => mid} = params) do
    redirect(conn, to: Routes.catering_catering_menu_path(conn, :update, cid, mid, params))
  end

  def delete(conn, %{"catering_id" => cid, "menu_id" => mid} = params) do
    redirect(conn, to: Routes.catering_catering_menu_path(conn, :delete, cid, mid, params))
  end

  def delete(conn, %{"id" => id}) do
    Server.Catering.delete_menu(id)

    render(conn, "index.html", menus: Server.Catering.list_menus)
  end
end
