defmodule ServerWeb.CateringOrderController do
  use ServerWeb, :controller

  def index(conn, %{"catering_id" => _cid} = params) do
    render(conn, "index.html", params)
  end

  def new(conn, %{"catering_id" => _cid} = params) do
    render(conn, "new.html", params)
  end

  def create(conn, %{"catering_id" => _cid} = params) do
    render(conn, "index.html", params)
  end

  def show(conn, %{"catering_id" => _cid, "order_id" => _oid} = params) do
    render(conn, "show.html", params)
  end

  def edit(conn, %{"catering_id" => cid, "order_id" => oid} = params) do
    redirect(conn, to: Routes.catering_catering_order_path(conn, :edit, cid, oid, params))
  end

  def update(conn, %{"catering_id" => cid, "order_id" => oid} = params) do
    redirect(conn, to: Routes.catering_catering_order_path(conn, :update, cid, oid, params))
  end

  def delete(conn, %{"catering_id" => cid, "order_id" => oid} = params) do
    redirect(conn, to: Routes.catering_catering_order_path(conn, :delete, cid, oid, params))
  end
end
