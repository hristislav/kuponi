defmodule ServerWeb.OrderController do
  use ServerWeb, :controller

  alias Server.Orders
  alias Server.Orders.Order
  alias Server.Accounts

  def index(conn, %{"parent_id" => parent_id}) do
    orders = Orders.get_orders_for_parent(parent_id)
    render(conn, "index.html", orders: orders)
  end

  def new(conn, _params) do
    changeset = Orders.change_order(%Order{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"child" => order_params}) do
    parent = Accounts.get_parent!(order_params["parent_id"])
    menu_price = String.to_float(order_params["menu_price"])

    if parent.money >= menu_price do
      money = parent.money - menu_price
      Accounts.update_parent(parent, %{"money" => money})

      case Orders.create_order(order_params) do
        {:ok, order} ->
          conn
          |> put_flash(:info, "Поръчката беше успешно създадена!")
          |> redirect(
            to: Routes.parent_parent_child_path(conn, :index, order_params["parent_id"])
          )
          |> halt()

        {:error, _else} ->
          conn
          |> put_flash(:error, "Грешка при изпълнението на поръчката!")
          |> redirect(to: Routes.menu_path(conn, :index))
          |> halt()
      end
    else
      conn
      |> put_flash(:error, "Нямате достатъчно пари в акаунта!")
      |> redirect(
        to:
          Routes.menu_path(conn, :index,
            child_id: order_params["child_id"],
            school_id: order_params["school_id"],
            parent_id: order_params["parent_id"]
          )
      )
      |> halt()
    end
  end

  def show(conn, %{"id" => id}) do
    order = Orders.get_order!(id)
    render(conn, "show.html", order: order)
  end

  def edit(conn, %{"id" => id}) do
    order = Orders.get_order!(id)
    changeset = Orders.change_order(order)
    render(conn, "edit.html", order: order, changeset: changeset)
  end

  def update(conn, %{"id" => id, "order" => order_params}) do
    order = Orders.get_order!(id)

    case Orders.update_order(order, order_params) do
      {:ok, order} ->
        conn
        |> put_flash(:info, "Order updated successfully.")
        |> redirect(to: Routes.order_path(conn, :show, order))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", order: order, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    order = Orders.get_order!(id)
    {:ok, _order} = Orders.delete_order(order)

    conn
    |> put_flash(:info, "Поръчката бе изтрита успешно!.")
    |> redirect(to: Routes.order_path(conn, :index))
  end

  def delete_order(conn, %{"parent_id" => parent_id, "price" => price, "order_id" => order_id}) do
    price = String.to_float(price)

    Orders.get_order!(order_id)
    |> Orders.delete_order()

    parent = Accounts.get_parent!(parent_id)
    parent_price = parent.money + price
    Accounts.update_parent(parent, %{"money" => parent_price})

    conn
    |> put_flash(:info, "Поръчката е изтрита и парите са върнати!")
    |> redirect(to: Routes.parent_order_path(conn, :index, parent_id))
  end

  def orders(conn, %{"catering_id" => catering_id}) do
    orders = Orders.get_orders_for_catering(catering_id)
    render(conn, "orders.html", orders: orders)
  end
end
