defmodule ServerWeb.RoomChannel do
  use Phoenix.Channel
  def join("rooms:lobby", _payload, socket) do
    {:ok,socket}
  end
  def join(_topic, _payload, _socket) do
    {:error,%{reason: "unauthorized"}}
  end
end
