defmodule Hash do
  def hash(data), do: :crypto.hash(:sha512, data) |> Base.encode32()

  def verify?(digest, data) do
    digest == hash(data)
  end
end
