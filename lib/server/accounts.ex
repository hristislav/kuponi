defmodule Server.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  import Comeonin.Bcrypt
  require Logger

  alias Server.Repo
  alias Ecto.{Changeset}
  alias Server.Accounts.{Credential, Parent, School, Child, Catering}
  alias Server.Addressess.Address
  alias Server.Auth.Guardian

  def sign_in(conn, email, password) do
    case email_password_auth(email, password) do
      {:ok, user_cred} ->
        user_type = String.to_atom(user_cred.type)
        user_preload = Repo.preload(user_cred, user_type)
        {:ok, user} = Map.fetch(user_preload, user_type)
        user = Map.put(user, :user_type, to_string(user_type))
        Guardian.Plug.sign_in(conn, user)

      {:error, error} ->
        Logger.warn("#{__MODULE__}.sing_in-err/#{error}")
        {:error, error}
    end
  end

  defp get_by_email(email) when is_binary(email) do
    repo = Repo.get_by(Credential, email: email)

    case repo do
      nil ->
        dummy_checkpw()
        {:error, "Login error."}

      user_cred ->
        {:ok, user_cred}
    end
  end

  defp verify_password(password, %Credential{} = cred) when is_binary(password) do
    if checkpw(password, cred.password_hash) do
      {:ok, cred}
    else
      {:error, :invalid_password}
    end
  end

  defp email_password_auth(email, password) when is_binary(email) and is_binary(password) do
    with {:ok, user_cred} <- get_by_email(email),
         do: verify_password(password, user_cred)
  end

  def get_address(id) do
    Repo.get!(Address, id)
  end

  def authenticate_account(username, password)
      when is_binary(username) and is_binary(password) do
    cred = Repo.get_by(Credential, username: username)

    cond do
      cred && Hash.verify?(cred.password_hash, password) ->
        type = String.to_atom(cred.type)
        cred = Repo.preload(cred, type)
        {:ok, {type, Map.get(cred, type).id}}

      cred ->
        {:error, :unauthorized}

      true ->
        {:error, :not_found}
    end
  end

  def list_credentials do
    Repo.all(Credential)
  end

  def get_credential!(id), do: Repo.get!(Credential, id)

  def create_credential(attrs \\ %{}) do
    %Credential{}
    |> Credential.registration_changeset(attrs)
  end

  def update_credential(%Credential{} = credential, attrs) do
    credential
    |> Credential.changeset(attrs)
    |> Repo.update()
  end

  def delete_credential_by_id(id) do
    Repo.get!(Credential, id) |> Repo.delete()
  end

  def delete_credential(%Credential{} = credential) do
    Repo.delete(credential)
  end

  def change_credential(%Credential{} = credential) do
    Credential.changeset(credential, %{})
  end

  alias Server.Accounts.Parent

  def list_parents do
    Repo.all(Parent)
  end

  def get_parent!(id) do
    Repo.get!(Parent, id)
    |> Repo.preload(:credential)
  end

  def create_parent(attrs \\ %{}) do
    {{city, number}, rest} =
      List.pop_at(Server.Addressess.list_cities(:names), String.to_integer(attrs["city"]) - 1)

    attrs1 = Map.replace!(attrs, "city", city)
    address = Map.replace!(attrs1["address"], "city", city) |> Map.replace!("province", city)
    Map.put(attrs1["credential"], "type", "parent")

    attrs1 =
      %{attrs1 | "address" => address}
      |> Map.put("money", 0.0)

    %Parent{}
    |> Parent.changeset(attrs1)
    |> Ecto.Changeset.cast_assoc(:address,
      required: true,
      with: &Server.Addressess.Address.changeset/2
    )
    |> Ecto.Changeset.cast_assoc(:credential,
      required: true,
      with: &Credential.registration_changeset/2
    )
    |> Repo.insert()
  end

  def update_parent(%Parent{} = parent, attrs) do
    parent
    |> Parent.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end

  def update_parent_money(%Parent{} = parent, attrs) do
    parent
    |> Parent.changeset(attrs)
    |> Repo.update()
  end

  def delete_parent(%Parent{} = parent) do
    Repo.delete(parent)
    delete_credential_by_id(parent.credential_id)
    Addressess.delete_address_by_id(parent.address_id)
  end

  def change_parent(%Parent{} = parent) do
    Parent.changeset(parent, %{})
  end

  def child_configmation(attrs \\ %{}) do
    attrs["egn"]
    |> get_child_by_egn()
    |> Child.child_confirmation_changeset(attrs)
    |> Repo.update()
  end

  defp get_child_by_egn(egn) do
    q =
      from(c in Child,
        where: c.egn == ^egn and c.active? == false,
        select: struct(c, [:id, :name, :egn, :rfid, :secure_code, :school_id])
      )

    case Repo.all(q) do
      [%Child{egn: ^egn} = child] -> child
      # {:error, :not_found}
      _ -> %Child{}
    end
  end

  alias Server.Accounts.School

  def list_schools do
    Repo.all(School)
  end

  def get_school!(id) do
    Repo.get!(School, id)
    |> Repo.preload(:credential)
  end

  def create_school(attrs \\ %{}) do
    attrs1 = attrs["registration"]
    name_num = String.to_integer(attrs1["city"])

    {{city, number}, rest} =
      List.pop_at(
        Server.Addressess.list_cities(:names),
        name_num - 1
      )

    attrs2 = Map.replace!(attrs1, "city", city)
    address = Map.replace!(attrs2["address"], "city", city) |> Map.replace!("province", city)
    test = Map.put(attrs2["credential"], "type", "school")
    attrs2 = %{attrs2 | "credential" => test}
    attrs2 = %{attrs2 | "address" => address}

    %School{}
    |> School.changeset(attrs2)
    |> Ecto.Changeset.cast_assoc(:address,
      required: true,
      with: &Server.Addressess.Address.changeset/2
    )
    |> Ecto.Changeset.cast_assoc(:credential,
      required: true,
      with: &Credential.registration_changeset/2
    )
    |> Repo.insert()
  end

  def update_school(%School{} = school, attrs) do
    school
    |> School.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end

  def delete_school(%School{} = school) do
    Repo.delete(school)
  end

  def change_school(%School{} = school) do
    School.changeset(school, %{})
  end

  alias Server.Accounts.Child

  def list_children_school_id(school_id) do
    %School{children: children} =
      school_id
      |> get_school!()
      |> Repo.preload(:children)

    children
  end

  def list_children do
    Repo.all(Child)
  end

  def get_child!(id) do
    Repo.get!(Child, id)
    |> Repo.preload(:parent)
    |> Repo.preload(:school)
  end

  def create_child(attrs \\ %{}) do
    %Child{}
    |> Child.create_changeset(attrs)
    |> Repo.insert()
  end

  def update_child(%Child{} = child, attrs) do
    child
    |> Child.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:parent_id,
      required: true,
      with: &Child.child_confirmation_changeset/2
    )
    |> Repo.update()
  end

  def delete_child(%Child{} = child) do
    Repo.delete(child)
  end

  def change_child(%Child{} = child) do
    Child.changeset(child, %{})
  end

  alias Server.Accounts.Catering

  def list_caterings do
    Repo.all(Catering)
  end

  def get_catering!(id) do
    Repo.get!(Catering, id)
    |> Repo.preload(:credential)
  end

  def create_catering(attrs \\ %{}) do
    {{city, number}, rest} =
      List.pop_at(Server.Addressess.list_cities(:names), String.to_integer(attrs["city"]) - 1)

    attrs1 = Map.replace!(attrs, "city", city)
    address = Map.replace!(attrs1["address"], "city", city) |> Map.replace!("province", city)
    test = Map.put(attrs1["credential"], "type", "catering")
    attrs1 = %{attrs1 | "credential" => test}
    attrs1 = %{attrs1 | "address" => address}

    %Catering{}
    |> Catering.changeset(attrs1)
    |> Ecto.Changeset.cast_assoc(:address,
      required: true,
      with: &Server.Addressess.Address.changeset/2
    )
    |> Ecto.Changeset.cast_assoc(:credential,
      required: true,
      with: &Credential.registration_changeset/2
    )
    |> Repo.insert()
  end

  def mfa(a, b) do
    &Credential.registration_changeset/2
  end

  def update_catering(%Catering{} = catering, attrs) do
    catering
    |> Catering.changeset(attrs)
    |> maybe_password_update(attrs)
    |> Repo.update()
  end

  defp maybe_password_update(
         %Changeset{valid?: true} = changeset,
         %{"credential" => %{"password" => _}}
       ) do
    changeset
    |> Ecto.Changeset.cast_assoc(:credential,
      with: &Credential.registration_changeset/2
    )
  end

  defp maybe_password_update(%Catering{} = catering, _attrs), do: catering

  def delete_catering(%Catering{} = catering) do
    Repo.delete(catering)
  end

  def change_catering(catering \\ %Catering{}) do
    catering
    |> Catering.changeset()
  end

  alias Server.Accounts.ChildConfirmation

  def list_child_confirmations do
    Repo.all(ChildConfirmation)
  end

  def create_child_confirmation(attrs \\ %{}) do
    %ChildConfirmation{}
    |> ChildConfirmation.changeset(attrs)
  end

  alias Server.Accounts.Host

  def list_hosts do
    Repo.all(Host)
  end

  def get_host!(id) do
    Repo.get!(Host, id)
    |> Repo.preload(:credential)
  end

  def create_host(attrs \\ %{}) do
    {{city, number}, rest} =
      List.pop_at(Server.Addressess.list_cities(:names), String.to_integer(attrs["city"]) - 1)

    attrs1 = Map.replace!(attrs, "city", city)
    address = Map.replace!(attrs1["address"], "city", city) |> Map.replace!("province", city)
    test = Map.put(attrs1["credential"], "type", "host")
    attrs1 = %{attrs1 | "credential" => test, "address" => address}

    %Host{}
    |> Host.changeset(attrs1)
    |> Ecto.Changeset.cast_assoc(:address,
      required: true,
      with: &Server.Addressess.Address.changeset/2
    )
    |> Ecto.Changeset.cast_assoc(:credential,
      required: true,
      with: &Credential.registration_changeset/2
    )
    |> Repo.insert()
  end

  def update_host(%Host{} = host, attrs) do
    host
    |> Host.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end

  def delete_host(%Host{} = host) do
    Repo.delete(host)
  end

  def change_host(%Host{} = host) do
    Host.changeset(host, %{})
  end

  alias Server.Accounts.Administrator

  def list_administrators do
    Repo.all(Administrator)
  end

  def get_administrator!(id), do: Repo.get!(Administrator, id)

  def create_administrator(attrs \\ %{}) do
    %Administrator{}
    |> Administrator.changeset(attrs)
    |> Repo.insert()
  end

  def update_administrator(%Administrator{} = administrator, attrs) do
    administrator
    |> Administrator.changeset(attrs)
    |> Repo.update()
  end

  def delete_administrator(%Administrator{} = administrator) do
    Repo.delete(administrator)
  end

  def change_administrator(%Administrator{} = administrator) do
    Administrator.changeset(administrator, %{})
  end
end
