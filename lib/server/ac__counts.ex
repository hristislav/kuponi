defmodule Server.Ac__counts do
  @moduledoc """
  The Ac__counts context.
  """

  import Ecto.Query, warn: false
  alias Server.Repo

  alias Server.Ac__counts.Us__er

  @doc """
  Returns the list of us__ers.

  ## Examples

      iex> list_us__ers()
      [%Us__er{}, ...]

  """
  def list_us__ers do
    Repo.all(Us__er)
  end

  @doc """
  Gets a single us__er.

  Raises `Ecto.NoResultsError` if the Us  er does not exist.

  ## Examples

      iex> get_us__er!(123)
      %Us__er{}

      iex> get_us__er!(456)
      ** (Ecto.NoResultsError)

  """
  def get_us__er!(id), do: Repo.get!(Us__er, id)

  @doc """
  Creates a us__er.

  ## Examples

      iex> create_us__er(%{field: value})
      {:ok, %Us__er{}}

      iex> create_us__er(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_us__er(attrs \\ %{}) do
    %Us__er{}
    |> Us__er.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a us__er.

  ## Examples

      iex> update_us__er(us__er, %{field: new_value})
      {:ok, %Us__er{}}

      iex> update_us__er(us__er, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_us__er(%Us__er{} = us__er, attrs) do
    us__er
    |> Us__er.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Us__er.

  ## Examples

      iex> delete_us__er(us__er)
      {:ok, %Us__er{}}

      iex> delete_us__er(us__er)
      {:error, %Ecto.Changeset{}}

  """
  def delete_us__er(%Us__er{} = us__er) do
    Repo.delete(us__er)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking us__er changes.

  ## Examples

      iex> change_us__er(us__er)
      %Ecto.Changeset{source: %Us__er{}}

  """
  def change_us__er(%Us__er{} = us__er) do
    Us__er.changeset(us__er, %{})
  end
end
