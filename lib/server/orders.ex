defmodule Server.Orders do
  @moduledoc """
  The Orders context.
  """

  import Ecto.Query, warn: false
  alias Server.Repo

  alias Server.Orders.Order
  alias Server.Accounts.Child
  alias Server.Catering.Menu

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders do
    Repo.all(Order)
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(id), do: Repo.get!(Order, id)

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    order
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order) do
    Repo.delete(order)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{source: %Order{}}

  """
  def change_order(%Order{} = order) do
    Order.changeset(order, %{})
  end

  def get_orders_for_parent(parent_id)do
    query = from o in Order, where: o.parent_id == ^parent_id
    orders = Repo.all(query)
    for x <- orders, into: [] do
      x
      |> Repo.preload(:parent)
      |> Repo.preload(:menu)
      |> Repo.preload(:child)
      |> Repo.preload(:catering)
      |> Repo.preload(:school)
    end
  end

  def get_orders_for_catering(catering_id)do
    query = from o in Order, where: o.catering_id == ^catering_id
    orders = Repo.all(query)
    for x <- orders, into: [] do
      x
      |> Repo.preload(:parent)
      |> Repo.preload(:menu)
      |> Repo.preload(:child)
      |> Repo.preload(:catering)
      |> Repo.preload(:school)
    end
  end

  def get_tag_order(tag) do

    case get_child_by_tag(tag) do
      
      [] -> []

      [list] -> 
        query = from o in Order, where: o.child_id == ^list.id
        case Repo.all(query) do

          [] -> []

          [order] -> 
            menu_id = order.menu_id
            query = from m in Menu, where: m.id == ^menu_id
            menu = 
              hd(Repo.all(query))
              |> Repo.preload(:food_main)
              |> Repo.preload(:food_dessert)
              |> Repo.preload(:food_starter)
            %{
              starter: %{
                "starter_name" => menu.food_starter.name,
                "starter_description" => menu.food_starter.description
              },
              main: %{
                "main_name" => menu.food_main.name,
                "main_description" => menu.food_main.description
              },
              dessert: %{
                "dessert_name" => menu.food_dessert.name,
                "dessert_description" => menu.food_dessert.description
              }
            }
            

        end

    end

  end

  def get_child_by_tag(tag) do
    query = from c in Child, where: c.rfid == ^tag
    Repo.all(query)
  end
end
