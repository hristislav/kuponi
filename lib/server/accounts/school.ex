defmodule Server.Accounts.School do
  use Ecto.Schema
  import Ecto.Changeset

  schema "schools" do
    field :name, :string
    # field :email, :string
    has_many :children, Server.Accounts.Child
    belongs_to :credential, Server.Accounts.Credential
    belongs_to :address, Server.Addressess.Address
    has_many :order, Server.Orders.Order
    timestamps()
  end

  @doc false
  def changeset(school, attrs) do
    school
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
