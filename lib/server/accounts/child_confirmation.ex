defmodule Server.Accounts.ChildConfirmation do
  use Ecto.Schema
  import Ecto.Changeset
  schema "child_confirmations" do
    field :egn, :string, virtual: true
    field :secure_code, :string, virtual: true
    field :confirmation_code, :string, virtual: true, defailt: nil

    timestamps()
  end

  @doc false
  def changeset(confirmation_form, attrs) do
    confirmation_form
    |> cast(attrs, [:egn, :secure_code])
    |> validate_required([:egn, :secure_code])
    |> validate_confirmation(:egn)
    |> validate_confirmation(:secure_code)
    |> validate_length(:egn, min: 10)
    |> validate_length(:egn, max: 10)
    |> gen_confirmations_code()
  end

  defp gen_confirmations_code(%{valid?: false} = confirmation_form), do: confirmation_form

  defp gen_confirmations_code(
         %{valid?: true, changes: %{egn: egn, secure_code: secure_code}} = confirmation_form
       ) do
    gencc = fn -> Hash.hash(egn <> secure_code) end

    put_change(confirmation_form, :confirmation_code, gencc.())
  end
end
