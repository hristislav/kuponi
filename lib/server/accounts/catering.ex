defmodule Server.Accounts.Catering do
  use Ecto.Schema
  import Ecto.Changeset

  schema "caterings" do
    field :name, :string
    # field :email, :string
    has_many :foods, Server.Catering.Food
    belongs_to :credential, Server.Accounts.Credential
    belongs_to :address, Server.Addressess.Address
    has_many :order, Server.Orders.Order
    timestamps()
  end

  @doc false
  def changeset(catering, attrs \\ %{}) do
    catering
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
