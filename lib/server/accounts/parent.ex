defmodule Server.Accounts.Parent do
  use Ecto.Schema
  import Ecto.Changeset

  schema "parents" do
    field :name, :string
    # field :email, :string
    field :money, :float
    has_many :children, Server.Accounts.Child
    belongs_to :credential, Server.Accounts.Credential
    belongs_to :address, Server.Addressess.Address
    has_many :order, Server.Orders.Order
    timestamps()
  end

  @doc false
  def changeset(parent, attrs) do
    parent
    |> cast(attrs, [:name, :money])
    |> validate_required([:name])
  end
end
