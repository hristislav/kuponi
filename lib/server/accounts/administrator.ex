defmodule Server.Accounts.Administrator do
  use Ecto.Schema
  import Ecto.Changeset

  schema "administrators" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(administrator, attrs) do
    administrator
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
