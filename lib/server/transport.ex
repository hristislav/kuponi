defmodule Server.Transport do
  require Logger

  alias Server.Orders

  @name __MODULE__
  @uuid UUID.uuid4()

  def child_spec(_options) do
    %{
      id: @name,
      name: @name,
      start: {@name, :start_link, []},
      shutdown: :brutal_kill,
      type: :worker
    }
  end

  def start_link() do
    params = []
    ## All values are default
    #  host: Connection host, charlist, default: 'localhost'
    #  port: Connection port, integer, default 1883 free port
    #  client_id: Binary ID for client, automatically set to UUID if not specified
    #  clean_sess: MQTT cleanSession flag. true disables persistent sessions on the server
    #  keepalive: Keepalive timer, integer
    #  username: Login username, binary
    #  password: Login password, binary
    #  will: Last will, keywordlist, sample: [qos: 1, retain: false, topic: "WillTopic", payload: "I died"]
    #  connack_timeout: Timeout for connack package, integer, default 60
    #  puback_timeout: Timeout for puback package, integer, default 8
    #  suback_timeout: Timeout for suback package, integer, default 4
    #  ssl: List of ssl options
    #  auto_resub: Automatically resubscribe to topics, boolean, default: false
    #  reconnect: Automatically reconnect on lost connection, integer (), default false
    mqtt_params = [
      # Should replace with external IP
      host: '192.168.0.100',
      port: 1883,
      #  client_id: :a,
      clean_sess: false,
      keepalive: 0,
      # username: "",
      # password: "",
      will: [qos: 1, retain: false, topic: "off", payload: @uuid],
      connack_timeout: 60,
      # puback_timeout: 8,
      # suback_timeout: 4,
      # ssl: [],
      auto_resub: true,
      reconnect: true
    ]

    link = {:ok, pid} = Exmqttc.start_link(__MODULE__, [], mqtt_params, params)
    Process.register(pid, @name)
    Logger.info("started ==> #{@name} #{inspect(link)}")
    self_subscribe()
    link
  end

  def init(params) do
    # %{opt: params}
    default_state = %{}

    {:ok,
     cond do
       {:ok, state} = init_state(params) ->
         state

       true ->
         default_state
     end}
  end

  # --------------------------------------- #
  # ----------------- API ----------------- #
  # --------------------------------------- #

  # def publush(payload) do
  #   GenServer.cast(@name, {:publush, payload})
  # end

  def subscribe(payload) do
    GenServer.cast(@name, {:subscribe, payload})
  end

  # -------------------------------------------- #
  # ----------------- HANDLERS ----------------- #
  # -------------------------------------------- #

  def handle_connect(state) do
    Logger.debug("Connected")
    {:ok, state}
  end

  def handle_disconnect(state) do
    Logger.debug("Disconnected")
    {:ok, state}
  end

  def handle_publish(_, payload, state) do
    {:ok, binary} = Base.decode64(payload)
    tag = :erlang.binary_to_term(binary).rfid_tag

    case Orders.get_tag_order(tag) do

      [] ->
        payload = %{response: %{error: "Детето не притежава платен купон!"}}
        ServerWeb.Endpoint.broadcast!("rooms:lobby", "get_lunch", %{payload: payload})

      map ->
        payload = %{response: map} 
        ServerWeb.Endpoint.broadcast!("rooms:lobby", "get_lunch", %{payload: payload})

    end

    ## TODO: Do the checks if there is a food fot this tag in the DB
    ## Then send the result by socket
    # ServerWeb.Endpoint.broadcast!("rooms:lobby", "get_lunch", %{payload: payload})
    {:ok, state}
  end

  # def handle_publish("/replay_get_school_list/" <> p_id, _payload, %{portal_id: p_id} = state) do
  #   # PortalWeb.Endpoint.broadcast!("room:lobby", "get_school_list", %{payload: payload})
  #   {:ok, state}
  # end

  # # ----------------- HANDLERS CALL ----------------- #
  # def handle_cast({:publush, payload}, %{portal_id: portal_id} = state) do
  #   Exmqttc.publish(@name, "portal/#{portal_id}", payload)
  #   {:ok, state}
  # end

  def handle_cast({:subscribe, payload}, %{portal_id: portal_id} = state) do
    # Exmqttc.subscribe(@name, "portal/#{state.portal_id}")
    Exmqttc.subscribe(@name, payload)
    {:ok, state}
  end

  # ------------------------------------------- #
  # ----------------- PRIVATE ----------------- #
  # ------------------------------------------- #
  defp init_state(kvp_list) do
    {:ok,
     %{
       portal_id: @uuid,
       assets: kvp_list
     }}
  end

  defp self_subscribe() do
    # Exmqttc.subscribe(@name, "/replay_get_lunch/#{state.portal_id}")
    Exmqttc.subscribe(@name, "#")
  end
end
