defmodule Server.Ac__counts.Us__er do
  use Ecto.Schema
  import Ecto.Changeset

  schema "us__ers" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(us__er, attrs) do
    us__er
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
