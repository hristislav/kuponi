defmodule Server.Orders.Order do
  use Ecto.Schema
  import Ecto.Changeset

  schema "orders" do
    belongs_to :parent, Server.Accounts.Parent
    belongs_to :menu, Server.Catering.Menu
    belongs_to :child, Server.Accounts.Child
    belongs_to :catering, Server.Accounts.Catering
    belongs_to :school, Server.Accounts.School

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:parent_id, :menu_id, :child_id, :catering_id, :school_id])
    |> validate_required([:parent_id, :menu_id, :child_id, :catering_id, :school_id])
  end
end
