defmodule Server.Credentials do

    import Ecto.Query, warn: false
    alias Server.Repo

    alias Server.Accounts.Credential

    def list_credentials() do
        Repo.all(Credential)
    end    

end