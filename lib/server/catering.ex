defmodule Server.Catering do
  @moduledoc """
  The Catering context.
  """

  import Ecto.Query, warn: false
  alias Server.Repo

  alias Server.Catering.FoodType
  alias Server.Catering.Food
  alias Server.Catering.Menu

  @doc """
  Returns the list of food_types.

  ## Examples

      iex> list_food_types()
      [%FoodType{}, ...]

  """
  def list_food_types do
    Repo.all(FoodType)
  end

  @doc """
  Gets a single food_type.

  Raises `Ecto.NoResultsError` if the Food type does not exist.

  ## Examples

      iex> get_food_type!(123)
      %FoodType{}

      iex> get_food_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_food_type!(id) do
    Repo.get!(FoodType, id)
  end

  @doc """
  Creates a food_type.

  ## Examples

      iex> create_food_type(%{field: value})
      {:ok, %FoodType{}}

      iex> create_food_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_food_type(attrs \\ %{}) do
    %FoodType{}
    |> FoodType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a food_type.

  ## Examples

      iex> update_food_type(food_type, %{field: new_value})
      {:ok, %FoodType{}}

      iex> update_food_type(food_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_food_type(%FoodType{} = food_type, attrs) do
    food_type
    |> FoodType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a FoodType.

  ## Examples

      iex> delete_food_type(food_type)
      {:ok, %FoodType{}}

      iex> delete_food_type(food_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_food_type(%FoodType{} = food_type) do
    Repo.delete(food_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking food_type changes.

  ## Examples

      iex> change_food_type(food_type)
      %Ecto.Changeset{source: %FoodType{}}

  """
  def change_food_type(%FoodType{} = food_type) do
    FoodType.changeset(food_type, %{})
  end

  

  def new_food(attrs) do
    Food.changeset(%Food{},attrs)
  end

  def delete_menu(id) do
    Repo.get!(Menu, id)
    |> Repo.delete()
  end

  def new_menu(attrs) do
    Menu.changeset(%Menu{},attrs)
    |> Repo.insert()
  end

  def list_menus do
    Repo.all(Menu)
  end

  def get_menu(id) do
    Menu
    |> Repo.get!(id)
  end

  @doc """
  Returns the list of foods.

  ## Examples

      iex> list_foods()
      [%Food{}, ...]

  """
  def list_foods do
    Repo.all(Food)
  end

  def get_starters(id) do
    query = from f in Food, where: f.food_type == "1" and f.catering_id == ^id
    Repo.all(query)
  end

  def get_main(id) do
    query = from f in Food, where: f.food_type == "2" and f.catering_id == ^id
    Repo.all(query)
  end

  def get_dessert(id) do
    query = from f in Food, where: f.food_type == "3" and f.catering_id == ^id
    Repo.all(query)
  end

  @doc """
  Gets a single food.

  Raises `Ecto.NoResultsError` if the Food does not exist.

  ## Examples

      iex> get_food!(123)
      %Food{}

      iex> get_food!(456)
      ** (Ecto.NoResultsError)

  """
  def get_food!(id) do
    Food
    |> Repo.get!(id)
    # |> transform_food_type()
  end

  defp transform_food_type(%Food{food_type: ft_id} = food) do
    structureless_food =
      food
      |> Map.from_struct()
      |> put_in([:food_type], Server.Catering.get_food_type!(ft_id).name)

    struct(%Food{}, structureless_food)
  end

  @doc """
  Creates a food.

  ## Examples

      iex> create_food(%{field: value})
      {:ok, %Food{}}

      iex> create_food(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_food(attrs \\ %{}) do
    %Food{}
    |> Food.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a food.

  ## Examples

      iex> update_food(food, %{field: new_value})
      {:ok, %Food{}}

      iex> update_food(food, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_food(%Food{} = food, attrs) do
    food
    |> Food.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Food.

  ## Examples

      iex> delete_food(food)
      {:ok, %Food{}}

      iex> delete_food(food)
      {:error, %Ecto.Changeset{}}

  """
  def delete_food(%Food{} = food) do
    Repo.delete(food)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking food changes.

  ## Examples

      iex> change_food(food)
      %Ecto.Changeset{source: %Food{}}

  """
  def change_food(%Food{} = food) do
    Food.changeset(food, %{})
  end
  
  def change_menu(%Menu{} = menu) do
    Menu.changeset(menu, %{})
  end

  

end
