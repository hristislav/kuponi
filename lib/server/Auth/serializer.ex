defmodule Server.Auth.Serializer do
  # @behaviour Guardian.Serializer

  alias Server.Repo
  alias Server.Accounts.{Administrator, Host, School, Parent, Catering, Credential}

  # Credential
  def for_token(user = %Administrator{}), do: {:ok, "Admin:#{user.id}"}
  def for_token(user = %Catering{}), do: {:ok, "Catering:#{user.id}"}
  def for_token(user = %Credential{}), do: {:ok, "Credential:#{user.id}"}
  def for_token(user = %Host{}), do: {:ok, "Host:#{user.id}"}
  def for_token(user = %School{}), do: {:ok, "School:#{user.id}"}
  def for_token(user = %Parent{}), do: {:ok, "Parent:#{user.id}"}
  def for_token(_), do: {:error, "Unknown resource type"}

  def from_token("Admin:" <> id), do: {:ok, Repo.get(Administrator, id)}
  def from_token("Catering:" <> id), do: {:ok, Repo.get(Catering, id)}
  def from_token("Credential:" <> id), do: {:ok, Repo.get(Credential, id)}
  def from_token("Host:" <> id), do: {:ok, Repo.get(Host, id)}
  def from_token("School:" <> id), do: {:ok, Repo.get(School, id)}
  def from_token("Parent:" <> id), do: {:ok, Repo.get(Parent, id)}
  def from_token(_), do: {:error, "Unknown resource type"}
end
