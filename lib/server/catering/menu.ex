defmodule Server.Catering.Menu do
  use Ecto.Schema
  import Ecto.Changeset
  schema "menus" do
    field :date, :string
    belongs_to :food_starter, Server.Catering.Food, foreign_key: :starter_id
    belongs_to :food_main, Server.Catering.Food, foreign_key: :main_id
    belongs_to :food_dessert, Server.Catering.Food, foreign_key: :dessert_id
    belongs_to :catering, Server.Accounts.Catering
    has_many :order, Server.Orders.Order
    # many_to_many :foods, Server.Catering.Food, join_through: "menus_foods"

    timestamps()
  end

  @doc false
  def changeset(menu, attrs) do
    menu
    |> cast(attrs, [:date, :starter_id, :main_id, :dessert_id, :catering_id])
    |> validate_required([:date])
  end
end
