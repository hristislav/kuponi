.PHONY: killall

###
### build commands
###
run: internal-build-run

up-run: internal-build-up

###
### internal commands
###
internal-build-run:
	@mix && iex -S mix phx.server

internal-build-up:
	@mix do deps.get, ecto.reset, ecto.migrate && mix
	@iex -S mix phx.server

purge: killall
	@rm -rf ./deps ./_build
	@mix clean

prod-release:
	@MIX_ENV=prod mix release --env=prod

killall:
	@echo "Kill all beam processes"
	@pkill -9 beam || true

setup_env:
	@echo "No environment variables"