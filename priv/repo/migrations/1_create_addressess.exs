defmodule Server.Repo.Migrations.CreateAddressess do
  use Ecto.Migration

  def change do
    create table(:addressess) do
      add :country, :string
      add :city, :string
      add :province, :string
      add :address, :string
      add :post_code, :string

      timestamps()
    end
  end
end
