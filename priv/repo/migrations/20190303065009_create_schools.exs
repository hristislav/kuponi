defmodule Server.Repo.Migrations.CreateSchools do
  use Ecto.Migration

  def change do
    create table(:schools) do
      add :name, :string
      # add :email, :string
      add :credential_id, references(:credentials)
      add :address_id, references(:addressess)
      timestamps()
    end
  end
end
