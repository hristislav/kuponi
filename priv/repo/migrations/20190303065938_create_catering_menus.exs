defmodule Server.Repo.Migrations.CreateCateringMenus do
  use Ecto.Migration

  def change do
    create table(:menus) do
      add :date, :string
      add(:starter_id, references(:foods))
      add(:main_id, references(:foods))
      add(:dessert_id, references(:foods))
      add(:catering_id, references(:caterings))
      timestamps()
    end
  end
end
