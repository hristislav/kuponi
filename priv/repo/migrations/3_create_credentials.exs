defmodule Server.Repo.Migrations.CreateCredentials do
  use Ecto.Migration

  def change do
    create table(:credentials) do
      add :email, :string
      add :username, :string
      add :password_hash, :string
      add :type, :string

      timestamps()
    end

    create unique_index(:credentials, [:username])
  end
end
