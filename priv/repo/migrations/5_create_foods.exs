defmodule Server.Repo.Migrations.CreateFoods do
  use Ecto.Migration

  def change do
    create table(:foods) do
      add :food_type, :string
      add :name, :string
      add :price, :float
      add :description, :text
      add(:catering_id, references(:caterings))
      # add(:menu_id, references(:menus))
      timestamps()
    end

    # create index(:foods, [:type])
  end
end
