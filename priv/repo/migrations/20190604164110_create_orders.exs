defmodule Server.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :parent_id, references(:parents)
      add :menu_id, references(:menus)
      add :child_id, references(:children)
      add :catering_id, references(:caterings)
      add :school_id, references(:schools)

      timestamps()
    end

  end
end
